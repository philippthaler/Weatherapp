# My Weatherapp

Made this app for the [curriculum](https://github.com/tylermcginnis/react-fundamentals-curriculum) for the React course by Tyler McGinnis.
<br>
You can visit the site [here](https://myweatherapp-edf71.firebaseapp.com/)
You can see his courses [here](https://tylermcginnis.com)
