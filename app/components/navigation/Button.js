import React from "react";
import { withRouter } from "react-router-dom";

const buttons = {
  backBtn: require("../../images/backBtn.svg"),
  forwardBtn: require("../../images/forwardBtn.svg")
};

function Button({ type, history }) {
  const goBack = () => {
    history.goBack();
  };
  const goForward = () => {
    history.goForward();
  };
  return (
    <button
      onClick={() => {
        type === "back" && goBack();
        type === "forward" && goForward();
      }}
    >
      <img src={buttons[type + "Btn"]} />
    </button>
  );
}

export default withRouter(Button);
