import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NotFound from "./NotFound";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Home from "./Home";
import Forecast from "./Forecast";
import Details from "./Details";

require.context("../images", true, /\.svg$/);

export default function App(props) {
  return (
    <Router>
      <div className="app">
        <Navbar />
        <div className="content-container">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/forecast" component={Forecast} />
            <Route path="/details" component={Details} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}
