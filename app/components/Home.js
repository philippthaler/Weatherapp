import React from "react";
import WeatherSearch from "./WeatherSearch";
import "../css/Home.css";

export default function Home(props) {
  return (
    <div className="home-container">
      <h1>Enter a City or a State</h1>
      <div className="home-search">
        <WeatherSearch>
          <p>Get Weather</p>
        </WeatherSearch>
      </div>
    </div>
  );
}
