import React from "react";
import PropTypes from "prop-types";
import IconComponent from "./IconComponent";
import "../css/ForecastDay.css";

export default function Details({
  location: {
    state: { data }
  }
}) {
  const absoluteZero = -273.15;
  console.log(data);
  return (
    <div className="details">
      <IconComponent data={data} />
      <p>{data.name}</p>
      <p>{data.weather.description}</p>
      <p>
        min. temperature: {Math.round(data.main.temp_min + absoluteZero)}
        °C
      </p>
      <p>
        max. temperature: {Math.round(data.main.temp_max + absoluteZero)}
        °C
      </p>
      <p>Humidity: {data.main.humidity}%</p>
    </div>
  );
}
