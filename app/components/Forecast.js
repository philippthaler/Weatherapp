import React from "react";
import queryString from "query-string";
import api from "../utils/api";
import ForecastDay from "./ForecastDay";
import Loading from "./Loading";
import "../css/Forecast.css";

export default class Forecast extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fiveDayForecast: "",
      error: "",
      loading: true
    };

    this.componentDidMount.bind(this);
  }

  initComponent() {
    const location = queryString.parse(this.props.location.search);
    api.getForecast(location.city).then(results => {
      if (results === null) {
        return this.setState(() => ({
          error: "Something went wrong",
          loading: false
        }));
      }

      this.setState(() => ({
        error: null,
        fiveDayForecast: results,
        loading: false
      }));
    });
  }

  componentDidMount() {
    this.initComponent();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.search !== this.props.location.search) {
      this.props = nextProps;
    }
    this.initComponent();
  }

  render() {
    const { error, loading, fiveDayForecast } = this.state;

    if (loading === true) {
      return (
        <div className="loading">
          <Loading />
        </div>
      );
    }
    if (error != null) {
      return (
        <div className="loading">
          <p>{error}</p>
        </div>
      );
    }

    const forecast = api.getDailyForecast(fiveDayForecast.data);
    return (
      <div>
        <h1 className="forecast-header">{fiveDayForecast.data.city.name}</h1>
        <div className="forecast-container">
          {forecast.map(day => {
            return <ForecastDay data={day} key={day.dt_txt} />;
          })}
        </div>
      </div>
    );
  }
}
