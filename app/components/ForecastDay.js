import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import IconComponent from "./IconComponent";

export default function ForecastDay({ data }) {
  return (
    <Link
      to={{
        pathname: `/details/${data.city}`,
        state: {
          name: data.city,
          data: data
        }
      }}
    >
      <IconComponent data={data} />
    </Link>
  );
}

ForecastDay.propTypes = {
  data: PropTypes.object.isRequired
};
