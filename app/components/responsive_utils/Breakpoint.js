import React from "react";
import PropTypes from "prop-types";
import MediaQuery from "react-responsive";

const breakpoints = {
  desktop: "(min-device-width: 851px)",
  mobile: "(max-device-width: 850px)"
};

function Breakpoint(props) {
  const breakpoint = breakpoints[props.name] || breakpoints.desktop;

  return (
    <MediaQuery {...props} query={breakpoint}>
      {props.children}
    </MediaQuery>
  );
}

Breakpoint.propTypes = {
  name: PropTypes.string,
  children: PropTypes.object
};

function DesktopBreakpoint(props) {
  return <Breakpoint name="desktop">{props.children}</Breakpoint>;
}

function MobileBreakpoint(props) {
  return <Breakpoint name="mobile">{props.children}</Breakpoint>;
}

export { DesktopBreakpoint, MobileBreakpoint };
